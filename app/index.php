<?php
$texto = "<span>Hello</span> World!!!";

?>

<!DOCTYPE html>
<html lang="pt-br">

  <head>
    <meta charset="UTF-8">

    <!-- Definindo a largura da pagina para seguir a largura da tela do dispositivo que abrir o site. -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

    <title> Projeto Piloto Devops </title>

    <style>
      .footer{color:#ccc;position:absolute;bottom:1%;width:100%;}.container-fluid{text-align:center;}
      span{color:#7968ab;}.col-md-12{font-size:60px;}h3{text-align:center;}
    </style>
  </head>

  <body>

    <!-- Container de largura total abrangendo toda a largura da viewport -->
    <div class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <?php
            echo $texto;
          ?>
        </div>
      </div>

        <div class="row">
	<div class="col-md-12 footer">
          <h3>Desenvolvido por <a href="http://www.concretesolutions.com.br" target="_blank"><img src="img/cs.png" width="200px"></a> </h3>
        </div>
	</div>
    </div>

  </body>


</html>
